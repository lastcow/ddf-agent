package agent.ddf.ifx.com;

import agent.ddf.ifx.com.util.ddfRestfulHelper;
import org.apache.commons.cli.*;

import static java.lang.System.exit;

/**
 * Created by chen3 on 11/23/16.
 */
public class ddfAgent {

    public ddfAgent(String... args){

        Options argOptions = new Options();

        // Check arguments
        argOptions.addOption("help", false, "Display this help");
        argOptions.addOption("o", true, "Options, file: upload a file, kw18: kw18 process, kml: kml process");
        argOptions.addOption("file", true, "Resource file attached to DDF server");
        argOptions.addOption("url", true, "DDF Server url, eg: http://ifxddl.com:8993");


        // Parsing
        CommandLineParser commandLineParser = new DefaultParser();
        try {
            CommandLine cmd = commandLineParser.parse(argOptions, args);

            if(cmd.getOptions().length == 0 || cmd.hasOption("help")){
                // Print help
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp( "ddfAgent", argOptions );
            }else {
                if (! (cmd.hasOption("o") && cmd.hasOption("file") && cmd.hasOption("url"))){
                    HelpFormatter formatter = new HelpFormatter();
                    formatter.printHelp("ddfAgent", argOptions);
                    exit(0);
                }

                String ddfUrl = cmd.getOptionValue("url");
                String fileName = cmd.getOptionValue("file");
                switch(cmd.getOptionValue("o")){
                    case "file":
                        ddfRestfulHelper.createRequestFileUpload(ddfUrl, fileName);
                        break;
                    case "kw18":
                        ddfRestfulHelper.createRequestKw18(ddfUrl, fileName);
                        break;
                    case "kml":
                        ddfRestfulHelper.createRequestKML(ddfUrl, fileName);
                        break;
                    default:
                        break;
                }

            }

            // Exit
            exit(0);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * Entrance
     * @param args
     */
    public static void main(String... args){

        new ddfAgent(args);
    }




}
