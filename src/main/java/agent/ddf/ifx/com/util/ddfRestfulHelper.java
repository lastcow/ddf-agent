package agent.ddf.ifx.com.util;

import agent.ddf.ifx.com.healer.AgentConstant;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by chen3 on 11/23/16.
 */
public class ddfRestfulHelper {


    /**
     * Upload single data file to DDF.
     * @param fileName
     */
    public static void createRequestFileUpload(String ddfUrl, String fileName){

        try{

            SSLContextBuilder builder = new SSLContextBuilder();
            builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());

            // Try to connect to DDF web service (ignore SSL)
            CloseableHttpClient httpClient = HttpClients.custom().setSSLContext(builder.build()).setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE).build();

            File file = new File(fileName);
            HttpPost httpPost = new HttpPost(ddfUrl + AgentConstant.UPLOAD_URI);
            FileBody fileBody = new FileBody(file, ContentType.DEFAULT_BINARY);

            MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();
            multipartEntityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            multipartEntityBuilder.addPart("upfile", fileBody);
            HttpEntity httpEntity = multipartEntityBuilder.build();

            httpPost.setEntity(httpEntity);
            HttpResponse httpResponse = httpClient.execute(httpPost);

            System.out.print("Status: " + httpResponse.getStatusLine().getStatusCode() + "\t");
            System.out.println("Response: " + httpResponse.getStatusLine().getReasonPhrase());

            if(httpResponse.getStatusLine().getStatusCode() == 201){
                System.out.println("Data file uploaded to DDF system");
            }


//            HttpGet httpGet = new HttpGet("https://192.168.1.8:8993/services/catalog/9746ea20ff5b492a8643dda71e1e9018");
//            System.out.println("Executing request " + httpGet.getRequestLine());
//
//            // Create a custom response handler
//            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
//
//                @Override
//                public String handleResponse(
//                        final HttpResponse response) throws ClientProtocolException, IOException {
//                    int status = response.getStatusLine().getStatusCode();
//                    if (status >= 200 && status < 300) {
//                        HttpEntity entity = response.getEntity();
//                        return entity != null ? EntityUtils.toString(entity) : null;
//                    } else {
//                        throw new ClientProtocolException("Unexpected response status: " + status);
//                    }
//                }
//
//            };
//            String responseBody = httpClient.execute(httpGet, responseHandler);
//            System.out.println("----------------------------------------");
//            System.out.println(responseBody);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }


    /**
     * Process KW18 file convert to file format that DDF can understand.
     * @param fileName
     */
    public static void createRequestKw18(String ddfUrl, String fileName){

        List<String> list = new ArrayList<>();
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            // Parsing kw18 file.

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process KML file convert to file format that DDF can understand.
     * @param fileName
     */
    public static void createRequestKML(String ddfUrl, String fileName){

    }
}
